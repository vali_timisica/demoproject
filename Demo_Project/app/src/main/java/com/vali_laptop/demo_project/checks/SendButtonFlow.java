package com.vali_laptop.demo_project.checks;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Vali_Laptop on 7/14/2017.
 */

public class SendButtonFlow {

    public boolean checkNrDigits(float number) {
        float length = (float) Math.log10(number) + 1;

        if (length == 10) {
            return true;
        }

        return false;
    }
}
