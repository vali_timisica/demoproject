package com.vali_laptop.demo_project.usersdatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Vali_Laptop on 7/14/2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String CONTACT_DATA_TABLE_CREATE =
            "CREATE TABLE " + ContactEntry.CONTACT_DATA_TABLE + " (" +
                    ContactEntry.PHONE_NUMBER + " TEXT PRIMARY KEY," +
                    ContactEntry.PASSWORD + " TEXT," +
                    ContactEntry.FIRST_NAME + " TEXT," +
                    ContactEntry.LAST_NAME + " TEXT," +
                    ContactEntry.EMAIL + " TEXT);";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + ContactEntry.CONTACT_DATA_TABLE;

    private static final String WHERE_CLAUSE = ContactEntry.PHONE_NUMBER + " = ?";

    public DBHelper(Context context) {
        super(context, ContactEntry.CONTACT_DATA_TABLE, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CONTACT_DATA_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void addContacts(String number, String pass, String firstName,
                            String lastName, String email) {
        SQLiteDatabase writer = getWritableDatabase();
        ContentValues newEntry = new ContentValues();
        if (writer != null) {
            newEntry.put(ContactEntry.PHONE_NUMBER, number);
            newEntry.put(ContactEntry.PASSWORD, pass);
            newEntry.put(ContactEntry.FIRST_NAME, firstName);
            newEntry.put(ContactEntry.LAST_NAME, lastName);
            newEntry.put(ContactEntry.EMAIL, email);
            writer.insert(ContactEntry.CONTACT_DATA_TABLE, null, newEntry);
            writer.close();
        }
    }

    public boolean checkIfExists(String number) {
        SQLiteDatabase reader = getReadableDatabase();
        Cursor cursor = reader.query(ContactEntry.CONTACT_DATA_TABLE,
                new String[]{ContactEntry.PHONE_NUMBER},
                WHERE_CLAUSE, new String[]{number},
                null, null, null);

        if (cursor.getCount() > 0) {
            return true;
        }

        return false;
    }

    public String getPassword(String number) {
        SQLiteDatabase reader = getReadableDatabase();
        Cursor cursor = reader.query(ContactEntry.CONTACT_DATA_TABLE,
                new String[]{ContactEntry.PASSWORD},
                WHERE_CLAUSE, new String[]{number},
                null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            int idx = cursor.getColumnIndex(ContactEntry.PASSWORD);
            return cursor.getString(idx);
        } else {
            return null;
        }
    }
}
