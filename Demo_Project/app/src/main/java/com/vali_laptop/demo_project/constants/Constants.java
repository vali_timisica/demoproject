package com.vali_laptop.demo_project.constants;

/**
 * Created by Vali_Laptop on 7/17/2017.
 */

public class Constants {

    public static final int PHONE_NR_LEN = 10;
    public static final String PASSWORD_KEY = "PASSWORD_KEY";
    public static final String PHONE_KEY = "PHONE_KEY";
}
