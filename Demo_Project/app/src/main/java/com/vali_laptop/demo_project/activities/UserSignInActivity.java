package com.vali_laptop.demo_project.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.vali_laptop.demo_project.R;
import com.vali_laptop.demo_project.checks.SendButtonFlow;
import com.vali_laptop.demo_project.constants.Constants;
import com.vali_laptop.demo_project.usersdatabase.DBHelper;

public class UserSignInActivity extends BaseActivity {

    EditText numberEditText;
    EditText passwordEditText;
    Button sendViaSmsButton;
    Button forgotPassButton;
    Button skipButton;
    SendButtonFlow sendButtonFlow;
    private DBHelper dbHelper;
    RelativeLayout relativeLayoutPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen1);

        numberEditText = (EditText) findViewById(R.id.et_phone_number);
        passwordEditText = (EditText) findViewById(R.id.et_password);

        sendViaSmsButton = (Button) findViewById(R.id.b_send_code_via_sms);
        forgotPassButton = (Button) findViewById(R.id.b_forgot_password);
        skipButton = (Button) findViewById(R.id.b_skip);

        relativeLayoutPass = (RelativeLayout) findViewById(R.id.rl_pass);

        dbHelper = new DBHelper(this);

        sendViaSmsButton.setOnClickListener(buttonClickListener);
        forgotPassButton.setOnClickListener(buttonClickListener);
        skipButton.setOnClickListener(buttonClickListener);

        sendButtonFlow = new SendButtonFlow();
    }

    private ButtonClickListener buttonClickListener = new ButtonClickListener();
    private class ButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Context context= view.getContext();
            String number = numberEditText.getText().toString();
            switch (view.getId()) {
                case R.id.b_send_code_via_sms:
                    if (!validateLength(number)) {
                        break;
                    }
                    if (validateDB(number)) {
                        if (relativeLayoutPass.getVisibility() == View.GONE) {
                            relativeLayoutPass.setVisibility(View.VISIBLE);
                        } else {
                            if (validatePass(number)) {
                                /*GOTO HomeScreenActivity*/
                                Intent intent = new Intent(getApplicationContext(), HomeScreenActivity.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(context, "Wrong password. Try again", Toast.LENGTH_LONG).show();
                            }
                        }
                    }

                    break;

                case R.id.b_forgot_password:
                    break;

                case R.id.b_skip:
                    /*GOTO HomeScreenActivity*/
                    Intent intent = new Intent(getApplicationContext(), HomeScreenActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    }

    private boolean validateLength(String number) {
        if (number.length() != Constants.PHONE_NR_LEN) {
            Toast.makeText(getApplicationContext(), "Phone number must have exactly 10 digits", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private boolean validatePass(String number) {
        String pass = dbHelper.getPassword(number);
        if (pass != null && pass.equals(passwordEditText.getText().toString())) {
            return true;
        }
        return false;
    }

    private boolean validateDB(String number) {
        if (!dbHelper.checkIfExists(number)) {
            /*GOTO SetPasswordActivity*/
            Intent intent = new Intent(getApplicationContext(), SetPasswordActivity.class);
            intent.putExtra(Constants.PHONE_KEY, number);
            startActivity(intent);
            return false;
        }
        return true;
    }
}
