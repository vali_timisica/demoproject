package com.vali_laptop.demo_project.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vali_laptop.demo_project.R;
import com.vali_laptop.demo_project.constants.Constants;

public class SetPasswordActivity extends BaseActivity {

    EditText newPassEditText;
    EditText newPassConfirmEditText;
    Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen2);

        newPassEditText = (EditText) findViewById(R.id.et_phone_number);
        newPassConfirmEditText = (EditText) findViewById(R.id.et_password);
        nextButton = (Button) findViewById(R.id.b_next);

        nextButton.setOnClickListener(nextButtonClickListener);
    }

    private ButtonClickListener nextButtonClickListener = new ButtonClickListener();
    private class ButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Context context= v.getContext();
            String pass = newPassEditText.getText().toString();
            String confirmPass = newPassConfirmEditText.getText().toString();

            if(validateFields(pass, confirmPass)) {
                Intent firstScreenIntent = getIntent();
                String number = firstScreenIntent.getStringExtra(Constants.PHONE_KEY);
                Intent intent = new Intent(getApplicationContext(), FillProfileActivity.class);
                intent.putExtra(Constants.PASSWORD_KEY, pass);
                intent.putExtra(Constants.PHONE_KEY, number);
                startActivity(intent);
            }
        }
    }

    private boolean validateFields(String pass, String confirmPass) {
        if (TextUtils.isEmpty(pass) || TextUtils.isEmpty(confirmPass)) {
            Toast.makeText(getApplicationContext(), "All fields are required", Toast.LENGTH_LONG).show();
            return false;
        }

        if (pass.length() < 8 || confirmPass.length() < 8) {
            Toast.makeText(getApplicationContext(), "Passwords must have at least 8 characters", Toast.LENGTH_LONG).show();
            return false;
        }

        if (!pass.equals(confirmPass)) {
            Toast.makeText(getApplicationContext(), "Passwords do not match", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }
}
