package com.vali_laptop.demo_project.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.vali_laptop.demo_project.R;
import com.vali_laptop.demo_project.constants.Constants;
import com.vali_laptop.demo_project.usersdatabase.DBHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FillProfileActivity extends BaseActivity {

    EditText firstNameEditText;
    EditText lastNameEditText;
    EditText emailEditText;
    Button nextButton;
    Button facebookButton;
    CallbackManager callbackManager;
    private DBHelper dbHelper;

    private String firstName, lastName, email;
    private String number, pass;
    private boolean added, emailValid;
    private String fbEmail, fbFirstName, fbLastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_screen3);
        callbackManager = CallbackManager.Factory.create();

        dbHelper = new DBHelper(this);

        firstNameEditText = (EditText) findViewById(R.id.et_first_name);
        lastNameEditText = (EditText) findViewById(R.id.et_second_name);
        emailEditText = (EditText) findViewById(R.id.et_email);
        nextButton = (Button) findViewById(R.id.b_next);
        facebookButton = (Button) findViewById(R.id.b_import_facebook);

        Intent dataIntent = getIntent();
        number = dataIntent.getStringExtra(Constants.PHONE_KEY);
        pass = dataIntent.getStringExtra(Constants.PASSWORD_KEY);

        nextButton.setOnClickListener(nextButtonClickListener);
        facebookButton.setOnClickListener(nextButtonClickListener);

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                /*Callback results*/
                                try {
                                    fbEmail = object.getString("email");
                                    fbFirstName = object.getString("first_name");
                                    fbLastName = object.getString("last_name");

                                    firstNameEditText.setText(fbFirstName);
                                    lastNameEditText.setText(fbLastName);
                                    emailEditText.setText(fbEmail);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                /*Send results*/
                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name,last_name,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(FillProfileActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(FillProfileActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private ButtonClickListener nextButtonClickListener = new ButtonClickListener();
    private class ButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Context context= v.getContext();
            switch (v.getId()) {
                case R.id.b_next:
                    firstName = firstNameEditText.getText().toString();
                    lastName = lastNameEditText.getText().toString();
                    email = emailEditText.getText().toString();

                    if (validateFields()) {
                        if(!added) {
                            dbHelper.addContacts(number, pass, firstName, lastName, email);
                            Toast.makeText(context, "Added in database", Toast.LENGTH_LONG).show();
                            added = true;
                        }
                        //GOTO scrren 4
                        Intent intent = new Intent(getApplicationContext(), HomeScreenActivity.class);
                        startActivity(intent);
                    }
                    break;

                case R.id.b_import_facebook:
                    LoginManager.getInstance().logInWithReadPermissions((Activity) context,
                                                    Arrays.asList("public_profile", "user_friends"));
                    break;
            }
        }
    }

    private boolean validateFields() {
        if (TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName) || TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "All fields are required", Toast.LENGTH_LONG).show();
            return false;
        }

        if(!isEmailValid(email)) {
            Toast.makeText(getApplicationContext(), "Wrong email format", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * method is used for checking valid email id format.
     */
    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}
