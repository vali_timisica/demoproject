package com.vali_laptop.demo_project.usersdatabase;

/**
 * Created by Vali_Laptop on 7/14/2017.
 */

public class ContactEntry {

    public static final String CONTACT_DATA_TABLE = "CONTACT_DATA_TABLE";
    public static final String PHONE_NUMBER = "PHONE_NUMBER";
    public static final String PASSWORD = "PASSWORD";
    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String LAST_NAME = "LAST_NAME";
    public static final String EMAIL = "EMAIL";
}
